#!/bin/bash
################################################################################
# Copyright (C) 2020 Alexander Pockes, congatec AG
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License version 2
# as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation, Inc., 
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
############################################################################################

#CGTLTWD_BIN="cgtCoLiWD"
WD_BIN="cgtCoLiWD-aarch64"
WD_DEV="/dev/watchdog0"
WD_TIMEOUT_SEC="10"
WD_SETUP_CMD="$WD_BIN -v0 -d $WD_DEV -s $WD_TIMEOUT_SEC"
WD_PING_CMD="$WD_BIN -v0 -d $WD_DEV -p"

if [ ! command -v "$WD_BIN" ]
then
    echo "ERR: $WD_BIN not in PATH, exiting"
    exit 1
fi

echo "Setting ${WD_DEV}'s timeout to $WD_TIMEOUT_SEC..."
if ! $WD_SETUP_CMD
then
    echo "ERR: Setting up WD timeout failed, exiting"
    exit 2
fi

for i in {19..10}
do
    echo "$i Pinging WD..."
    if ! $WD_PING_CMD
    then
        echo "ERR: Setting up WD timeout failed, exiting"
        exit 3
    fi
    sleep 1
done

for i in {9..0}
do
    echo "$i seconds until WD reset..."
    sleep 1
done

exit 0
