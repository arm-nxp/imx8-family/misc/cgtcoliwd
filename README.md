# cgtCoLiWD - README

cgtCoLiWD is the default congatec tool to Configure the Linux
WatchDog driver.

## Components

- cgtCoLiWD.c
    source code of a simple command line utility to access Linux'
    watchdog interface, e.g. /dev/watchdog0

- cgtTestWD.sh
    simple bash script to test the system watchdog utilizing
    cgtCoLiWD. First, it sets the WD timeout to 10s. Afterwards,
    it pings/resets the WD counter and waits a second.
    This ping procedure is done 10 times in a row.
    Finally, it counts down from 9 to 0 until watchdog reset is
    triggered.

## How To CrossCompile cgtCoLiWD (AARCH64)

```
source /PATH/TO/YOUR/COPY/OF/environment-setup-aarch64-poky-linux
aarch64-poky-linux-gcc -Wall --sysroot=$SDKTARGETSYSROOT -o cgtCoLiWD-aarch64 cgtCoLiWD.c
```
