/* ************************************************************************/ /**
 * cgtCoLiWD.c
 * *****************************************************************************
 * cgtCoLiWD is the congatec standard tool to Configure the Linux WatchDog:
 * it enables interaction with watchdog device files, e.g. /dev/watchdog0,
 * from commandline without circumstances.  
 * *****************************************************************************
 * *****************************************************************************
 * @author      Alexander Pockes (ATP)   <alexander.pockes@congatec.com>
 *
 * @copyright   Copyright 2020 Alexander Pockes, congatec AG
 *
 * @license     cgtCoLiWD is free software: you can redistribute it and/or
 *              modify it under the terms of the GNU General Public License
 *              version 2 as published by the Free Software Foundation.
 *
 *              cgtCoLiWD is distributed in the hope that it will be useful,
 *              but WITHOUT ANY WARRANTY; without even the implied warranty of
 *              MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *              GNU General Public License for more details.
 *
 *              You should have received a copy of the GNU General Public
 *              License along with this program.
 *              If not, see <http://www.gnu.org/licenses/>.
 * *****************************************************************************
 * *****************************************************************************
 *
 * ************************************************************************/ /**
 **************************************************************************** */

#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>

// required by getopt_long()
#include <getopt.h>
#include <unistd.h>

// required by open()
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

// watchdog related
#include <sys/ioctl.h>
#include <linux/watchdog.h>


//
// constants
//

// version information
#define VERSION_MAJOR "0"
#define VERSION_MINOR "2-1"

// error/return codes
#define ERR_NEVER_HAPPEN        255
#define ERR_NO_CMDL_OPT         254
#define ERR_MISS_CMDL_OPT       253
#define ERR_NO_ACT_SEL          252
#define ERR_NULL_PTR            251
#define ERR_NVALID_RET          250
#define ERR_OPEN_DEVICEFILE     249
#define ERR_CLOSE_DEVICEFILE    248
#define ERR_IOC                 1

// frequently used strings
#define INFO_STR "INFO:\t"
#define SUCC_STR "OK:\t"
#define ERR_STR "ERROR:\t"
#define DEB_STR "\nDEBUG:\t"


//
// types
//

// log level related
enum e_log_levels
{
    LL_QUIET = 0,
    LL_DEFAULT = 1,
    LL_MED = 2,
    LL_HIGH = 3,
    LL_DEBUG = 4,
};

struct log_level
{
    enum e_log_levels current_ll;
};
typedef struct log_level log_level;

#define fprintf_ll(CUR_LOG_LEVEL, stream, ...)            \
    if( CUR_LOG_LEVEL <= global_log_level.current_ll ) \
        fprintf(stream, __VA_ARGS__)

// other types
struct presources
{
    int *fd_dev;
};
typedef struct presources presources;


//
// globals
//
const char *eLine = "--------------------------------------------------------------------------------";
log_level global_log_level;


//
// prototypes...
//
void print_usage( int argc, char *argv[] );
void print_version_info( int argc, char *argv[] );
int clean_up( int err_code, presources *res );


//
// program logic
//

int main( int argc, char *argv[] )
{
    //
    // <used by getopt_long>
    //
    int gol_option = 0;                         // retval from getopt_long
    int gol_option_index = 0;                   // getopt_long stores the option index here
    struct option gol_long_options[] =          // defining cmdl options (long)
    {
          {"test",              no_argument,        0, 'z'},
          {"help",              no_argument,        0, 'h'},
          {"version",           no_argument,        0, 'i'},
          {"verbose",           optional_argument,  0, 'v'},
          {"device",            required_argument,  0, 'd'},
          {"settimeout",        required_argument,  0, 's'},
          {"setpretimeout",     required_argument,  0, 't'},
          {"gettimeout",        no_argument,        0, 'g'},
          {"keepalive",         no_argument,        0, 'p'},
          {0, 0, 0, 0}
    };


    //
    // actions needed for getopt interpretation
    //
    enum select_action
    {
        no_action,
        test,
        wdioc_settimeout,
        wdioc_setpretimeout,
        wdioc_gettimeout,
        wdioc_keepalive,
    } select_action;
    select_action = no_action;

    //
    // general
    //
    int retval = 0;                             // tmp return value

    //
    // devicefile related
    //
    int fd_dev = 0;                             // devicefile's file descriptor
    char *dev_default = "/dev/watchdog0";       // default device to use
    char *dev = dev_default;

    //
    // flags
    //
    int flag_open_devfile = 0;                  // opening devicefile (1) or not (0)

    //
    // command line arguments
    //
    int arg_verbosity = -1;                     // default log_level == LL_DEFAULT (1)
    int arg_timeout = -1;
    int arg_pretimeout = -1;

    // set default log level (verbosity)
    global_log_level.current_ll = LL_DEFAULT;

    //
    // all resources we have to care about must be part of presources
    //
    presources res = (presources) {
        .fd_dev = &fd_dev,
    };

    //
    // watchdog related
    //
    int wdioc_data_exchange = -1;

    //
    // <getopt_long interpretation>
    //
    while(1)
    {
        gol_option = getopt_long ( argc,
                                   argv,
                                   "d:hiv::zgps:t:",
                                   gol_long_options,
                                   &gol_option_index );

        // getopt_long returns -1, if there is no option left
        if ( gol_option == -1 )
            break;

        switch ( gol_option )
        {

        // general options
        case 'd':
            dev = optarg;

            break;

        case 'h':
            print_usage(argc, argv);
            clean_up(0, &res);
            break;

        case 'i':
            print_version_info(argc, argv);
            clean_up(0, &res);
            break;

        case 'v':
            if ( NULL == optarg )
                global_log_level.current_ll = LL_MED;
            else
            {
                sscanf( optarg, "%i", &arg_verbosity );
                if ( arg_verbosity < LL_QUIET || arg_verbosity > LL_DEBUG )
                {
                    fprintf( stderr, "%s invalid log level\n", ERR_STR );
                    fprintf( stderr, "Valid log levels:\n" );
                    fprintf( stderr, "\t  quiet:           -v0 | --verbose=0\n" );
                    fprintf( stderr, "\t  default:         -v1 | --verbose=1\n" );
                    fprintf( stderr, "\t  medium:     -v | -v2 | --verbose=2\n" );
                    fprintf( stderr, "\t  high:            -v3 | --verbose=3\n" );
                    fprintf( stderr, "\t  debug:           -v4 | --verbose=4\n" );
                    clean_up( ERR_MISS_CMDL_OPT, &res );
                }
                global_log_level.current_ll = arg_verbosity;
            }
            break;

        case 'z':
            select_action = test;
            break;

        // watchdog related
        case 'g':
            select_action = wdioc_gettimeout;

            fprintf_ll( LL_DEFAULT,
                        stdout,
                        "%s try to get wdog timeout...\n",
                        INFO_STR );

            flag_open_devfile = 1;
            break;

        // watchdog related
        case 'p':
            select_action = wdioc_keepalive;

            fprintf_ll( LL_DEFAULT,
                        stdout,
                        "%s try to ping wdog...\n",
                        INFO_STR );

            flag_open_devfile = 1;
            break;

        // watchdog related
        case 's':
            select_action = wdioc_settimeout;

            sscanf( optarg, "%i", &arg_timeout );
            fprintf_ll( LL_DEFAULT,
                        stdout,
                        "%s try to set wdog timeout to %i seconds...\n",
                        INFO_STR,
                        arg_timeout );

            flag_open_devfile = 1;
            break;

        // watchdog related
        case 't':
            select_action = wdioc_setpretimeout;

            sscanf( optarg, "%i", &arg_pretimeout );
            fprintf_ll( LL_DEFAULT,
                        stdout,
                        "%s try to set wdog pretimeout to %i seconds...\n",
                        INFO_STR,
                        arg_pretimeout );

            flag_open_devfile = 1;
            break;

        case '?':
            // getopt_long already printed an error message
            break;

        default:
            clean_up( ERR_NVALID_RET, &res );
            break;
        }
    }

    if( argc == 1 )                   // no cmdl arguments given...
    {
        print_usage( argc, argv );
        clean_up( ERR_NO_CMDL_OPT, &res );
    }


    //
    // <programm logic>
    //

    // opening devfile if flag_open_devfile has been set
    if( flag_open_devfile )
    {
        fprintf_ll( LL_DEFAULT, 
                    stdout,
                    "%s using devicefile: %s\n",
                    INFO_STR,
                    dev );
        fd_dev = open(dev, O_RDWR /*| O_NONBLOCK */);  // returns -1 on error
        if( !(fd_dev > 0) )
        {
            perror( "ERROR: \t Not able to open devicefile" );
            clean_up( ERR_OPEN_DEVICEFILE, &res );
        }
    }

    // actions
    if ( select_action == test )
    {
        fprintf_ll( LL_DEBUG,
                    stdout,
                    "%s test action selected\n",
                    INFO_STR );
        clean_up( 0, &res );
    }
    else if ( select_action == wdioc_settimeout )
    {
        // watchdog related
        fprintf_ll( LL_DEBUG,
                    stdout,
                    "%s wdioc_settimeout selected\n",
                    INFO_STR );

        wdioc_data_exchange = arg_timeout;
        retval = ioctl( fd_dev, WDIOC_SETTIMEOUT, &wdioc_data_exchange );
        if( retval != 0 )
        {
            perror( "ERROR: \t ioctl call failed, exiting" );
            clean_up( ERR_IOC, &res );
        }
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s called IOCTL with success!\n",
                    SUCC_STR );

        clean_up( 0, &res );
    }
    else if ( select_action == wdioc_setpretimeout )
    {
        // watchdog related
        fprintf_ll( LL_DEBUG,
                    stdout,
                    "%s wdioc_setpretimeout selected\n",
                    INFO_STR );

        wdioc_data_exchange = arg_pretimeout;
        retval = ioctl( fd_dev, WDIOC_SETPRETIMEOUT, &wdioc_data_exchange );
        if( retval != 0 )
        {
            perror( "ERROR: \t ioctl call failed, exiting" );
            clean_up( ERR_IOC, &res );
        }
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s called IOCTL with success!\n",
                    SUCC_STR );

        clean_up( 0, &res );
    }
    else if ( select_action == wdioc_gettimeout )
    {
        // watchdog related
        fprintf_ll( LL_DEBUG,
                    stdout,
                    "%s wdioc_gettimeout selected\n",
                    INFO_STR );

        retval = ioctl( fd_dev, WDIOC_GETTIMEOUT, &wdioc_data_exchange );
        fprintf_ll( LL_QUIET,
                    stdout,
                    "%i",
                    wdioc_data_exchange );

        if( retval != 0 )
        {
            perror( "ERROR: \t ioctl call failed, exiting" );
            clean_up( ERR_IOC, &res );
        }
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s called IOCTL with success!\n",
                    SUCC_STR );

        clean_up( 0, &res );
    }
    else if ( select_action == wdioc_keepalive )
    {
        // watchdog related
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s wdioc_keepalive selected\n",
                    INFO_STR );

        retval = ioctl( fd_dev, WDIOC_KEEPALIVE, &wdioc_data_exchange );
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s wdog ping sent\n",
                    INFO_STR );

        if( retval != 0 )
        {
            perror( "ERROR: \t ioctl call failed, exiting" );
            clean_up( ERR_IOC, &res );
        }
        fprintf_ll( LL_DEFAULT,
                    stdout,
                    "%s called IOCTL with success!\n",
                    SUCC_STR );

        clean_up( 0, &res );
    }
    else /* select_action == no_action */
    {
        fprintf( stderr,
                 "%s given arguments do not trigger an action, exiting...\n",
                 ERR_STR );

        clean_up ( ERR_NO_ACT_SEL, &res );
    }


    //
    // should never be executed
    // cleaning up...
    //
    clean_up ( ERR_NEVER_HAPPEN, &res );

    return ERR_NEVER_HAPPEN;
}


int clean_up( int err_code, presources *res )
{
    int tmp_ret = 0;

    // cleaning up...
    if( *(res->fd_dev) > 0 )
    {
        tmp_ret = close( *(res->fd_dev) );      // returns 0 on success
        if ( tmp_ret != 0 )
        {
            perror("ERROR: \t Not able to close devicefile");
            err_code = ERR_CLOSE_DEVICEFILE;
        }
    }

    err_code = err_code < 0 ? -err_code: err_code;
    exit( err_code );
}

void print_version_info( int argc, char *argv[] )
{
    fprintf(stderr, "%s\n", eLine);
    fprintf(stderr, "%s v%s.%s \n", argv[0],
            VERSION_MAJOR, VERSION_MINOR);
    fprintf(stderr, "%s\n", eLine);
    fprintf(stderr, "\n");
}

void print_usage( int argc, char *argv[] )
{
    fprintf(stderr, "%s\n", eLine);
    fprintf(stderr, "Usage: %s [option [argument]]  \n", argv[0]);
    fprintf(stderr, "%s\n", eLine);
    fprintf(stderr, "\n");

    fprintf(stderr, "watchdog access:\n");
    fprintf(stderr, "%s\n",eLine);
    fprintf(stderr, "-d DEVFILE, --device DEVFILE"
            "\n\t specify device-file to use, default /dev/watchdog0\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-g, --gettimeout"
            "\n\t get watchdog timeout; most-likely n.a.\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-p, --keepalive"
            "\n\t ping watchdog; resets timeout\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-s TIMEOUT, --settimeout TIMEOUT"
            "\n\t set watchdog timeout to TIMEOUT seconds\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-t PRETIMEOUT, --setpretimeout PRETIMEOUT"
            "\n\t set watchdog pretimeout to PRETIMEOUT seconds\n");
    fprintf(stderr, "\n");

    fprintf(stderr, "general options:\n");
    fprintf(stderr, "%s\n",eLine);
    fprintf(stderr, "-h, --help"
            "\n\t print this help\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-i, --version"
                "\n\t print version info\n");
    fprintf(stderr, "\n");
    fprintf(stderr, "-v [LOGLEVEL], --verbose[=LOGLEVEL]"
            "\n\t determine verbosity:");
    fprintf(stderr, "\n\t     quiet:           -v0 | --verbose=0");
    fprintf(stderr, "\n\t     default:         -v1 | --verbose=1");
    fprintf(stderr, "\n\t     medium:     -v | -v2 | --verbose=2");
    fprintf(stderr, "\n\t     high:            -v3 | --verbose=3");
    fprintf(stderr, "\n\t     debug:           -v4 | --verbose=4");
    /*\n\t 0 (quiet), 1 (default), 2 (medium), 3 (high), 4 (debug)\n");*/
    fprintf(stderr, "\n");
}
